" levarg/vimtex
let g:vimtex_log_ignore=[
      \'Command terminated with space.\n',
      \]
let g:vimtex_compiler_progname='nvr'
let g:vimtex_compiler_latexmk = {
    \ 'options' : [
    \   '-xelatex',
    \   '-shell-escape',
    \   '-verbose',
    \   '-file-line-error',
    \   '-synctex=1',
    \   '-interaction=nonstopmode',
    \ ],
    \}
