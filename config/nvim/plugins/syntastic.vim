" vim-syntastic/syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_nasm_nasm_args = '-f elf64'
let g:syntastic_cpp_compile = 'clang++'
let g:syntastic_cpp_config_file = '.clang_complete'
let g:syntastic_cpp_check_header = 1
let g:syntastic_cpp_auto_refresh_includes = 1
let g:syntastic_tex_chktex_quiet_messages = {
      \'regex':[
        \'Command terminated with space'
        \]
      \}
let g:syntastic_tex_lacheck_quiet_messages = {
      \'regex': '\Vpossible unwanted space at'
      \}
