" bling/vim-airline
let g:airline_powerline_fonts = 1
let g:airline_exclude_preview = 0
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tagbar#enabled = 0
