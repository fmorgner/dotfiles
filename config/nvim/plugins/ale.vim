inoremap <silent> <C-Space> <C-\><C-O>:ALEComplete<CR>

let g:ale_c_parse_compile_commands = 1
let g:ale_c_build_dir_names = ['build', 'bin']
let g:ale_cpp_clang_options = '-Wno-ignored-optimization-argument'
let g:ale_cpp_gcc_options = ''
let g:ale_cpp_ccls_init_options = {
\   'cache': {
\       'directory': '/tmp/ccls/cache',
\   },
\ }
let g:ale_completion_enabled = 1

let g:ale_python_auto_pipenv = 1
