" Haskell stuff
augroup ft_haskell
  autocmd!
  autocmd FileType haskell let g:haskellmode_completion_ghc = 0
  autocmd FileType haskell setlocal omnifunc=necoghc#omnifunc
  autocmd FileType haskell let g:ycm_semantic_triggers = {'haskell' : ['.']}
  autocmd FileType haskell compiler ghc
  autocmd FileType haskell onoremap <silent> ia :<c-u>silent execute "normal! ?->\r:nohlsearch\rwvf-ge"<CR>
  autocmd FileType haskell onoremap <silent> aa :<c-u>silent execute "normal! ?->\r:nohlsearch\rhvEf-ge"<CR>
  autocmd FileType haskell nnoremap <silent><buffer> git :GhcModTypeInsert<CR>
  autocmd FileType haskell nnoremap <silent><buffer> gfs :GhcModSplitFunCase<CR>
  autocmd FileType haskell nnoremap <silent><buffer> gtt :GhcModType<CR>
augroup END

" C++ stuff
augroup ft_cpp
  autocmd!
  autocmd FileType cpp autocmd BufWritePre <buffer> :%s/\s\+$//e
  autocmd FileType cpp let &colorcolumn=129
  autocmd FileType cpp let g:formatdef_nothing = '"nobody_should_write_this_program"'
  autocmd FileType cpp let g:formatters_cpp = ['nothing']
augroup END

" LaTeX stuff
let g:tex_flavor='latex'
augroup ft_tex
  autocmd!
  autocmd FileType tex,bib let &colorcolumn=81
  autocmd FileType tex,bib set spelllang=en
  autocmd FileType tex,bib set spell
augroup END

augroup txt_docs
  autocmd!
  autocmd FileType rst,markdown let &colorcolumn=81
  autocmd FileType rst,markdown setl spelllang=en
  autocmd FileType rst,markdown setl spell
augroup END

augroup ft_asm
  autocmd!
  autocmd BufRead,BufNewFile *.asm set filetype=nasm
augroup END

augroup ft_python
  autocmd!
augroup END

augroup filetypedetect
  au BufRead,BufNewFile *.ebnf set filetype=ebnf
augroup END
