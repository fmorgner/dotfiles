runtime plugins.vim
runtime plugins/*.vim
runtime mappings.vim
runtime filetype.vim
runtime projects.vim
runtime colorschemes.vim

" Automatically reload vimrc on changes
autocmd! bufwritepost init.vim source %

" Automatically disable paste mode when leavin insert mode
autocmd InsertLeave * set nopaste

" :W saves using sudo
command! W w !sudo tee % > /dev/null

" Enable filetype related plugins
set foldmethod=syntax
set foldlevel=99

" Syntax highlighting on
syntax on

" Set /tmp as the directory for swap files
set backupdir=/var/tmp
set directory=/var/tmp

" Highlight the search term while typing
set hlsearch
set incsearch
set showmatch

" Show the cursor position
set ruler
set cursorline

" Keep the cursor horizontally centered when possible
set scrolloff=999

" Show line numbers
set number
set laststatus=2
set numberwidth=3

" Hightlight trailing whitespace as errors
match Error /\s\+$/

" Basic indentation rules
set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab
set cino=f1s,{1s,}0,l1,b,b0,h1s,i1s,t0,>1s,:1s,(1s

set guifont=Source\ Code\ Pro\ 9

if(empty($TMUX) && $COLORTERM == "truecolor" || $TERM_PROGRAM =~ "iTerm")
  if(has("termguicolors"))
    set termguicolors
  endif
endif

let g:tex_conceal = ""

set shortmess=I
