call plug#begin('~/.local/share/nvim/plugged')

" LSP support
Plug 'w0rp/ale'

" Web
Plug 'diepm/vim-rest-console'
Plug 'vim-scripts/django.vim'
Plug 'elzr/vim-json'

" C++
Plug 'arakashic/chromatica.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'pboettch/vim-cmake-syntax'

" Utility
Plug 'VundleVim/Vundle.vim'
Plug 'nathanaelkane/vim-indent-guides'
Plug 'airblade/vim-gitgutter'
Plug 'bling/vim-airline'
Plug 'scrooloose/nerdtree'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'prabirshrestha/async.vim'

" Python

" Colorschemes
Plug 'dkasak/gruvbox'

" Misc
Plug 'sirtaj/vim-openscad'
Plug 'jamessan/vim-gnupg'
Plug 'lervag/vimtex'
Plug 'nfnty/vim-nftables'
Plug 'Shougo/vimproc.vim'
Plug 'Chiel92/vim-autoformat'
Plug 'vim-scripts/ebnf.vim'
Plug 'ossobv/vim-rst-tables-py3'
Plug 'darfink/vim-plist'
Plug 'airblade/vim-rooter'
Plug 'othree/xml.vim'
Plug 'Rykka/riv.vim'
Plug 'Rykka/InstantRst'
Plug 'rodjek/vim-puppet'

" OrgMode
Plug 'jceb/vim-orgmode'
Plug 'tpope/vim-speeddating'
Plug 'mattn/calendar-vim'
Plug 'inkarkat/vim-SyntaxRange'
Plug 'majutsushi/tagbar'

" Haskell
Plug 'eagletmt/neco-ghc'
Plug 'alx741/ghc.vim'
Plug 'eagletmt/ghcmod-vim'
Plug 'enomsg/vim-haskellConcealPlus'
Plug 'neovimhaskell/haskell-vim'
Plug 'alx741/vim-hindent'

" Nim
Plug 'zah/nim.vim'

" Java
Plug 'artur-shaik/vim-javacomplete2'

" Kotlin
Plug 'udalov/kotlin-vim'

call plug#end()
