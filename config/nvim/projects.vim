" uMurmur
augroup umurmur
  au!
  au BufRead,BufNewFile $HOME/Projects/C/umurmur*/**.[ch] setl ft=c
  au BufRead,BufNewFile $HOME/Projects/C/umurmur*/**.[ch] setl noexpandtab
  au BufRead,BufNewFile $HOME/Projects/C/umurmur*/**.[ch] setl tabstop=4
  au BufRead,BufNewFile $HOME/Projects/C/umurmur*/**.[ch] setl shiftwidth=4
  au BufRead,BufNewFile $HOME/Projects/C/umurmur*/**.[ch] setl cino=>1s,:1s,l1,t0,(1s ")
  " >1s,f0s,{0,}0,l1,b,b0,h1s,i1s,t0,:1s,(1s
augroup END

" Sophia Template Library
aug sophia
  au!
  au BufRead,BufNewFile $HOME/Projects/C++/misc/sophia/**.dox setl ft=cpp
aug END

" Constificator documentation
aug constificator_doc
  au!
  au BufRead,BufNewFile $HOME/Projects/Latex/constificator-doc/**.tex let g:vimtex_main = "constificator.tex"
  au BufRead,BufNewFile $HOME/Projects/Latex/constificator-doc/**.tex let g:vimtex_latexmk_build_dir = ".build"
aug END

" Linux Kernel Headers
aug kernel_headers
  au!
  au BufRead,BufNewFile /lib/modules/**/*.h set ft=c
aug END

" The CYBER device
aug dev-cyber
  au!
  au BufRead,BufNewFile $HOME/Projects/C/dev-cyber/**.[ch] setl ft=c
  au BufRead,BufNewFile $HOME/Projects/C/dev-cyber/**.[ch] setl cino=:0,l1,t0,g0,(0 ")
  au BufRead,BufNewFile $HOME/Projects/C/dev-cyber/**.[ch] setl noexpandtab
  au BufRead,BufNewFile $HOME/Projects/C/dev-cyber/**.[ch] setl tabstop=4
  au BufRead,BufNewFile $HOME/Projects/C/dev-cyber/**.[ch] setl shiftwidth=4
  au BufRead,BufNewFile $HOME/Projects/C/dev-cyber/**.[ch] setl softtabstop=4
aug END

" Lecture Notes and Exercises
aug MSE
  au!
  au BufRead,BufNewFile $HOME/Documents/MSE/**/lecture/**.md setl spelllang=de
  au BufRead,BufNewFile $HOME/Documents/MSE/**/exercise/**.md setl spelllang=de
aug END

" Bsys Lectures and Exercises
aug Bsys
  au!
  au BufRead,BufNewFile $HOME/**/bsys*/**.tex setl spelllang=de
  au BufRead,BufNewFile $HOME/**/bsys*/**.ltx setl spelllang=de
aug END

" CUTE
augroup cute
  au!
  au BufRead,BufNewFile $HOME/Projects/C++/misc/cute/**.cpp setl noexpandtab
  au BufRead,BufNewFile $HOME/Projects/C++/misc/cute/**.h setl noexpandtab
  au BufRead,BufNewFile $HOME/Projects/C++/misc/cute/**.cpp setl tabstop=4
  au BufRead,BufNewFile $HOME/Projects/C++/misc/cute/**.h setl tabstop=4
  au BufRead,BufNewFile $HOME/Projects/C++/misc/cute/**.cpp setl shiftwidth=4
  au BufRead,BufNewFile $HOME/Projects/C++/misc/cute/**.h setl shiftwidth=4
augroup END
