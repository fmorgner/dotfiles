" Set the leader key to , for easier access
let mapleader=","

" move lines up and down in normal mode
nnoremap <up> :m .-2<CR>==
nnoremap <down> :m .+1<CR>==

" indent lines in normal mode
nnoremap <left> <<
nnoremap <right> >>

" Move blocks up and down in visual mode
vnoremap <up> :m '<-2<CR>gv=gv
vnoremap <down> :m '>+1<CR>gv=gv

" Don't do anything in insert mode with the arrow keys
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>

" move "file lines" not "display lines"
nnoremap j gj
nnoremap k gk

" Quickly insert lines above or under "without" entering insert mode
nnoremap <Leader>o o<ESC>
nnoremap <Leader>O O<ESC>

" Quickly insert line above or below during insert mode
inoremap <Leader>o <ESC>o
inoremap <Leader>O <ESC>O

" Clear search-highlighting with L-Space
nnoremap <Leader><Space> :noh<CR>

" Don't lose selection when indendint in visual mode
vnoremap < <gv
vnoremap > >gv

" Toggle "cursor-centering"
nnoremap <Leader>zz :let &scrolloff=999-&scrolloff<CR>

" ------------------------------
" Function key mappings
" ------------------------------

" Press F2 for toggling the NERDtree
nnoremap <F2> :NERDTreeToggle<CR>
inoremap <F2> <ESC>:NERDTreeToggle<CR>

" Press F3 for paste mode
set pastetoggle=<F3>

" Press F6 to reindent the hole file
" map <F6> mzgg=G`zmz
map <F6> :Autoformat<CR>

nmap <Leader>hs <Plug>GitGutterStageHunk
nmap <Leader>gl :GitGutterLineHighlightsToggle<CR>

" Press <Leader>g to jump to declaration
map <Leader>g :YcmCompleter GoToDeclaration<CR>

" Press <Leader>f to jump to definition
nmap <Leader>f :YcmCompleter GoToDefinition<CR>

" Press <Leader>t to get the type of the expression under the cursor
nmap <Leader>t :YcmCompleter GetType<CR>

" Press <F8> to go one buffer back
nmap <F8> :bp<CR>

" Press <F9> to go one buffer forward
nmap <F9> :bn<CR>

" Press <F10> to close the current buffer
nmap <F10> :bd<CR>
