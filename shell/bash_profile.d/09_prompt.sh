# Enable the bash_completion scripts, depending on the distribution
# Also enable git prompt for use in PS1
if [ -f "/etc/profile.d/bash_completion.sh" ]; then
  . "/etc/profile.d/bash_completion.sh"

  if [[ -f "/usr/share/bash-completion/git-prompt" ||
    -f "/usr/share/bash-completion/git-prompt.sh" ||
    -f "/usr/share/bash-completion/completions/git" ]]; then
  GIT_PROMPT_LOADED=1
  fi
fi

if [ -f "/usr/share/git/completion/git-prompt.sh" ]; then
  . "/usr/share/git/completion/git-prompt.sh"
  GIT_PROMPT_LOADED=1
fi

# Include git status in the PS1 if available
if [[ $GIT_PROMPT_LOADED -eq '1' ]]; then
  PS1='\[\e[0;32m\]\u\[\e[0;34m\]@\H \[\e[1;31m\][\[\e[0;33m\]\W\[\e[0;34m\]$(__git_ps1 "{%s}")\[\e[1;31m\]] \[\e[1;34m\]\$\[\e[m\] '
else
  PS1="\[\e[0;32m\]\u\[\e[0;34m\]@\H \[\e[1;31m\][\[\e[0;33m\]\W\[\e[1;31m\]] \[\e[1;34m\]\$\[\e[0;0m\] "
fi
