# Nim - nimble
if hash nimble 2>/dev/null; then
  export PATH="${HOME}/.nimble/bin:${PATH}"
fi

# Android - tools & platform-tools
if [[ -d "${HOME}/Android/Sdk" ]]; then
  export ANDROID_HOME="${HOME}/Android/Sdk"
  export PATH="${ANDROID_HOME}/tools:${ANDROID_HOME}/platform-tools:${PATH}"
fi

# Haskell - Cabal
if hash cabal 2>/dev/null; then
  export PATH="${PATH}:${HOME}/.cabal/bin"
fi

# Go
if hash go 2>/dev/null; then
  export GOPATH=${HOME}/.gopath
  export PATH="${GOPATH}/bin:${PATH}"
fi

# Node.js - npm
if (npm config get prefix &>/dev/null); then
  export PATH="$(npm config get prefix)/bin:${PATH}"
fi

# Node.js - nvm
if [ -f "/usr/share/nvm/init-nvm.sh" ]; then
  source /usr/share/nvm/init-nvm.sh
fi

# Ruby
if hash ruby 2>/dev/null; then
  export PATH="$(ruby -e 'print Gem.user_dir')/bin:${PATH}"
fi

# Rust - cargo
if hash cargo 2>/dev/null; then
  export PATH="${HOME}/.cargo/bin:${PATH}"
fi
