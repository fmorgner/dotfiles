# Set vim as the default editor if available
if hash vim 2>/dev/null; then
  export EDITOR=vim
  export GIT_EDITOR=vim
  export SUDO_EDITOR=vim
  export GIT_EDITOR=vim
elif hash nvim 2>/dev/null; then
  export EDITOR=nvim
  export VISUAL=nvim
  export SUDO_EDITOR=nvim
  export GIT_EDITOR=nvim
fi
