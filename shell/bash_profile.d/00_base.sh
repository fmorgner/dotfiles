# Try to fix spelling mistakes in cd commands
shopt -s cdspell

# Enable asynchronous job completion notifications
set -o notify

# Don't record commands prefixed with a ' ' and don't record duplicates
HISTCONTROL=ignorespace:erasedups:ignoredups

# User home raltive binary path
_usr_path=${HOME}/.bin:${HOME}/.local/bin

# Local packages binary path
_loc_path=/usr/local/bin:/usr/local/sbin

# Source .bashrc if it exists
[[ -f $HOME/.bashrc ]] && . ~/.bashrc

# Export the default path
export PATH="${_usr_path}:${_loc_path}:${PATH}"
