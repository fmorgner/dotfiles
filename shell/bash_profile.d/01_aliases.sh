# Make ls convenient and colored
if ls --color &>/dev/null; then
  alias ls='ls --color=auto'
else
  alias ls='ls -G'
fi
alias ll='ls -l'
alias la='ll -all'

# Enable colors in grep
alias grep='grep --color=auto'

# Simplify open on XDG systems
if hash xdg-open &>/dev/null; then
  function do_open()
  {
    path=$(readlink -m "${1}")
    if [ -d "${path}" ]; then
      xdg-open "file://${path}/"
    elif [ -f "${path}" ]; then
      xdg-open "file://${path}"
    else
      echo "No such file or directory: \"${1}\""
    fi
  }

  alias open='do_open'
fi


# Default to VIM or neovim for VI
if hash vim 2>/dev/null; then
  alias vi='vim'
elif hash nvim 2>/dev/null; then
  alias vim='nvim'
fi

# Default to pydf if available
if hash pydf 2>/dev/null; then
  alias df='pydf'
else
  alias df='df -h'
fi
