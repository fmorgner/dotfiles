if [[ "$(uname)" != "Darwin" ]]; then
  echo "WRONG OS DETECTED! expected Darwin but was $(uname)"
fi

BREW_PREFIX=$(brew --prefix 2>/dev/null)

function do_full_upgrade() {
  echo "Updating homebrew repository... "
  brew update
  echo "Checking for outdated packages..."
  brew_outdated=$(brew outdated)
  if [[ -n "${brew_outdated}" ]]; then
    echo "Updating homebrew packages..."
    brew upgrade
  fi
  echo "Checking for outdated casks..."
  cask_outdated=$(brew cask outdated)
  if [[ -n "${cask_outdated}" ]]; then
    echo "Updating cask packages..."
    brew cask upgrade
  fi
}


if [ "$BREW_PREFIX" != "" ]; then
  alias full-upgrade='do_full_upgrade'

  if [ -f "$BREW_PREFIX/etc/bash_completion" ]; then
    . "$BREW_PREFIX/etc/bash_completion"

    if [ -f "$BREW_PREFIX/etc/bash_completion.d/git-prompt.sh" ]; then
      . $BREW_PREFIX/etc/bash_completion.d/git-prompt.sh
      GIT_PROMPT_LOADED=1
    fi
  fi
fi
