# Check if we a really on Linux
if [[ "$(uname)" != "Linux" ]]; then
  echo "WRONG OS DETECTED! expected Linux but was $(uname)"
fi

# Detect the distribution
[ -e /etc/os-release ] && export _DISTRIBUTION="$(sed -n 's/ID=\(\.*\)/\1/p' /etc/os-release)"

# Define the IncludeOS prefix
[ -d /usr/local/includeos/ ] && export INCLUDEOS_PREFIX=/usr/local

# Set less as the default pager if available
hash less 2>/dev/null && export PAGER=less

# Set most as the pager for man if available
hash most 2>/dev/null && export MANPAGER=most

export GIT_SSH=/usr/bin/ssh

# If we are in a truecolor terminal outside a tmux, enable xterm-256color
[ "${COLORTERM}" == "truecolor" ] && [ -z "$TMUX" ] && TERM=xterm-256color

# Handle distribution specific settings
case "${_DISTRIBUTION}" in
  arch*)
    # Set the ABS root
    [ -d ${HOME}/ABS ] && export ABSROOT=${HOME}/ABS
    # Convenience update alias
    alias full-upgrade='yay -Syyuu --noconfirm --removemake --devel'
    ;;
esac

# Remote DM-Crypt unlock for virtual machines
if hash secret-tool 2>/dev/null; then
  function do_ssh_unlock()
  {
    vm=$1

    if [ -z "${vm}" ]; then
      echo "missing parameter <vm>"
      echo "ssh_unlock <vm>"
    else
      if hash secret-tool 2>/dev/null; then
        secret-tool lookup dhvm ${vm} | ssh ${vm}_unlock sh -c 'cat > /lib/cryptsetup/passfifo'
      else
        echo "secret-tool is not installed!"
      fi
    fi
  }
  alias ssh_unlock='do_ssh_unlock'
fi

# Add bash completion for Django if available
if [ -f '/usr/share/django-bash-completion/django-bash-completion.sh' ]; then
  source /usr/share/django-bash-completion/django-bash-completion.sh
fi

if [[ -z "${SSH_AUTH_SOCK}" ]]; then
  if ! pgrep -U $(whoami) gpg-agent &>/dev/null; then
    if gpg-agent --daemon; then
      export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
    fi
  else
    export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
  fi
fi
